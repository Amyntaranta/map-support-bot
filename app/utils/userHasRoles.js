const userHasRole = require('./userHasRole')

const userHasRoles = (user, roles = []) => {
  let result = false
  for (let role of roles) {
    if (userHasRole(user, role)) {
      result = true
    }
  }
  return result
}

module.exports = userHasRoles
