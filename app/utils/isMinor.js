const readJSON = require('./readJSON')

const isMinor = user => {
  let json = readJSON('./minors.json')

  if (Object.keys(json).includes(user._id)) {
    return true
  } else {
    return false
  }
}

module.exports = isMinor
